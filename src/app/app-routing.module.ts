import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProgramsComponent } from './programs/programs.component';
import { EntrepriseComponent } from './entreprise/entreprise.component';
import { StudentComponent } from './student/student.component';
import { AboutComponent } from './about/about.component';
import { FaqComponent } from './faq/faq.component';
import { FacilitationComponent } from './facilitation/facilitation.component';
import { SignupComponent } from './signup/signup.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'programs', component: ProgramsComponent },
  {path: 'entreprise', component: EntrepriseComponent },
  {path: 'student', component: StudentComponent },
  {path: 'about', component: AboutComponent },
  {path: 'faq', component: FaqComponent },
  {path: 'form entreprise', component: SignupComponent },
  {path: 'form', component: FacilitationComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
