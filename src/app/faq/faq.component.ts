import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  panelOpenState = false ;
  etat = false ;
  step = 0;
  etatEntreprise = false ;
  etatEntreprise1 = false ;
  etatFinancials = false ;
  etatFinancials1 = false ;


  constructor() { }

  ngOnInit() {

  }
  panelEntrepriseone() {
    this.etatEntreprise = !this.etatEntreprise ;
    if (this.etatEntreprise1 === true) {
      this.etatEntreprise1 = !this.etatEntreprise1;
    }
  }
  panelEntreprisetwo() {
    this.etatEntreprise1 = !this.etatEntreprise1 ;
    if (this.etatEntreprise === true) {
      this.etatEntreprise = !this.etatEntreprise;
    }
  }
  panelFinancialsone() {
    this.etatFinancials = !this.etatFinancials ;
    if (this.etatFinancials1 === true) {
      this.etatFinancials1 = !this.etatFinancials1;
    }
  }
  panelFinancialstwo() {
    this.etatFinancials1 = !this.etatFinancials1 ;
    if (this.etatFinancials === true) {
      this.etatFinancials = !this.etatFinancials;
    }
  }

  opened() {
    this.panelOpenState = !this.panelOpenState ;
    if (this.etat === true) {
      this.etat = !this.etat;
    }
  }
  closed() {
    this.etat = !this.etat ;
    if (this.panelOpenState === true) {
      this.panelOpenState = !this.panelOpenState;
    }
  }


  setStep(index: number) {
    this.step = index;
  }
  nextStep() {
    this.step++;
  }
  prevStep() {
    this.step--;
  }
}
