import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitationComponent } from './facilitation.component';

describe('FacilitationComponent', () => {
  let component: FacilitationComponent;
  let fixture: ComponentFixture<FacilitationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
