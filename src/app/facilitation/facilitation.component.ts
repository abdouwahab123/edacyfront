import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-facilitation',
  templateUrl: './facilitation.component.html',
  styleUrls: ['./facilitation.component.scss']
})
export class FacilitationComponent implements OnInit {

  email: FormControl;
  phonegroup: FormGroup;

  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];

  countries: any;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.email = new FormControl('', [Validators.required, Validators.email]);
  }

getErrorMessage() {
  return this.email.hasError('required') ? 'You must enter a value' :
      this.email.hasError('email') ? 'Not a valid email' :
          '';
}

}
