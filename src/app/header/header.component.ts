import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  etat = false;
  Langues: FormControl;
  LANGUES: any[] = [
    {
      name: "FRENCH",
      code: "FR"
    },
    {
      name: "ENGLISH",
      code: "EN"
    }
  ];
  defaultLangue: string;
  constructor() { }

  ngOnInit() {
    this.Langues = new FormControl();
    this.defaultLangue = this.LANGUES[1].code;
  }
  show() {
    this.etat = !this.etat;
  }

}
